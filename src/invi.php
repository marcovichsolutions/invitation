<?php

namespace Marcovichsolutions\Invi;

/**
 * The MIT License (MIT)
 *
 * Copyright (c) 2016 Atbox
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND,
 * EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS
 * OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN
 * AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH
 * THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
 *
 * @package    Invi
 * @version    0.8.13 l5
 * @author     Sajjad Rad [sajjad.273@gmail.com]
 * @license    MIT License (3-clause)
 * @copyright  (c) 2016
 * @link       http://atbox.io/pages/opensource
 */

use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Str;

class Invi
{

    public static function forge()
    {
        $newClass = __CLASS__;
        return new $newClass();
    }
    public function generate($email,$expire,$active, $type, $payload = null)
    {
        if($this->checkEmail($email, $type))
        {
            $now = strtotime("now");
            $format = 'Y-m-d H:i:s ';
            $expiration = date($format, strtotime('+ '.$expire, $now)); 
            $code = Str::random(8) . $this->hash_split(hash('sha256',$email)) . $this->hash_split(hash('sha256',time())) . Str::random(8);
            $newInvi = array(
                    "code"          => $code,
                    "email"         => $email,
                    "expiration"    => $expiration,
                    "active"        => $active,
                    "type"          => $type,
                    "used"          => "0",
                    "payload"       => ($payload) ? json_encode($payload) : null
                );
            $Invi = array(
                    "code"          => $code,
                    "email"         => $email,
                    "expiration"    => $expiration,
                    "active"        => $active,
                    "type"          => $type,
                    "used"          => "0",
                    "payload"       => $payload
                );          
            Invitation::create($newInvi);
            return json_encode($Invi);
        }
        else
        {
            return json_encode(array(
                    "error" =>  "This email address has an invitation of type '$type'"
                ));
        }
        
    }
    public function unexpire($code,$email,$expire, $type)
    {
        $now = strtotime("now");
        $format = 'Y-m-d H:i:s ';
        $expiration = date($format, strtotime('+ '.$expire, $now)); 
        Invitation::where('code','=',$code)
                ->where('email','=',$email)
                ->where('type','=',$type)
                ->update(array('expiration'=>$expiration));
    }
    public function active($code,$email, $type)
    {
        Invitation::where('code','=',$code)
                ->where('email','=',$email)
                ->where('type','=',$type)
                ->update(array('active'=>True));
    }

    public function getByCode($code)
    {
        return Invitation::where('code','=',$code)->first();
    }

    public function deactive($code,$email,$type)
    {
        Invitation::where('code','=',$code)
                ->where('email','=',$email)
                ->where('type','=',$type)
                ->update(array('active'=>False));
    }
    public function used($code,$email, $type)
    {
        Invitation::where('code','=',$code)
                ->where('type','=',$type)
                ->where('email','=',$email)
                ->update(array('used'=>True));
    }
    public function unuse($code,$email, $type)
    {
        Invitation::where('code','=',$code)
                ->where('type','=',$type)
                ->where('email','=',$email)
                ->update(array('used'=>False));
    }
    public function status($code,$email, $type)
    {
        $temp = Invitation::where('code', '=', $code)->where('email','=',$email)
                    ->where('type','=',$type)
                    ->first();
        if($temp)
        {
            if(!$temp->active)
                return "deactive";
            else if($temp->used)
                return "used";
            else if(strtotime("now") > strtotime($temp->expiration))
                return "expired";
            else
                return "valid";
        }
        else
            return "not exist";
    }
    public function check($code,$email, $type)
    {
        $temp = Invitation::where('code', '=', $code)
                    ->where('email','=',$email)
                    ->where('type','=',$type)
                    ->first();
        if($temp)
        {
            if(!$temp->active or $temp->used or strtotime("now") > strtotime($temp->expiration))
                return False;
            else
                return True;
        }
        else
            return False;
    }
    public function delete($code,$email, $type)
    {
        $temp = Invitation::where('code', '=', $code)
                    ->where('email','=',$email)
                    ->where('type','=',$type)
                    ->delete();
    }
    public function emailStatus($email, $type)
    {
        $temp = Invitation::where('email','=',$email)
                    ->where('type','=',$type)
                    ->first();
        if($temp)
        {
            $expired = false;
            if(strtotime("now") > strtotime($temp->expiration))
                $expired = true;
            $invite = array(
                    "code"          => $temp->code,
                    "email"         => $temp->email,
                    "expiration"    => $temp->expiration,
                    "expired"       => $expired,
                    "type"          => $temp->type,
                    "active"        => $temp->active,
                    "used"          => $temp->used,
                    "payload"       => $temp->payload
                );
            return json_encode($invite);
        }
        else
            return False;
    }
    protected function checkEmail($email, $type)
    {
        $temp = Invitation::where('email', '=', $email)
               ->where('type', '=', $type)
               ->first();
        if($temp)
            return False;
        else
            return True;
    }

    protected function hash_split($hash)
    {
        $output = str_split($hash,8);
        return $output[rand(0,1)];
    }
}
