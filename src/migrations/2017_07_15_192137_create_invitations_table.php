<?php
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateInvitationsTable extends Migration {

    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
            Schema::table('invitations', function($table) {
                $table->engine = 'InnoDB';
                $table->create();
                $table->increments('id')->unsigned();   
                $table->text('code', 255);
                $table->string('email');
                $table->date('expiration');
                $table->boolean('active');
                $table->boolean('used')->default(False);
                $table->string('type');
                $table->text('payload')->nullable();
                $table->timestamps();
            });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('invitations', function($table) {
            $table->drop();
        });
    }

}
