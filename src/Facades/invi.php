<?php

namespace Marcovichsolutions\Invi\Facades;

use Illuminate\Support\Facades\Facade;
 
class Invi extends Facade {
 
  /**
   * Get the registered name of the component.
   *
   * @return string
   */
  protected static function getFacadeAccessor() { return 'invi'; }
 
}