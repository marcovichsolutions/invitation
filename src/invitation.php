<?php

namespace Marcovichsolutions\Invi;

use Illuminate\Database\Eloquent\Model;

class Invitation extends Model
{
    protected $table = 'invitations';
    protected $fillable = array('code','email','expiration','active','used', 'payload', 'type');
}